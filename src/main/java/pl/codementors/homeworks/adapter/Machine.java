package pl.codementors.homeworks.adapter;

public interface Machine {

    boolean run();

    boolean stop();
}
