package pl.codementors.homeworks.adapter;

public class CarToMachineAdapter implements Machine {

    private Car car;

    public CarToMachineAdapter(Car car){
        this.car = car;
    }

    public boolean run() {
        this.car.start();
        return true;
    }

    public boolean stop() {
        this.car.stop();
        return true;
    }
}
