package pl.codementors.homeworks.builder;

public class PathSelectorBuilder {

    private String formGroup;

    private String span;

    private String userMessage;

    private String formControl;

    public PathSelectorBuilder withFormGroup(String formGroup) {
        this.formGroup = "[@class='"+formGroup+"']";
        return this;
    }

    public PathSelectorBuilder withSpan(String span) {
        this.span = span;
        return this;
    }

    public PathSelectorBuilder withUserMessage(String userMessage) {
        this.userMessage = "[@id='"+userMessage+"']";
        return this;
    }

    public PathSelectorBuilder withFormControl(String formControl) {
        this.formControl= "[@class='"+formControl+"']";
        return this;
    }

    String build(){
        StringBuilder stringBuilder = new StringBuilder("/div/");
        int insertStringIntoDiv = "/div".length();

        stringBuilder.insert(insertStringIntoDiv, (formGroup == null) ? "" : formGroup)
                .append((span == null) ? "" : span)
                .append((userMessage == null) || (span == null) ? "" : userMessage)
                .append((formControl == null) || (span == null) ? "" : formControl);

        if(stringBuilder.length()>50){
            int indexToChange = stringBuilder.indexOf("][");
            int changeLength = "][".length();
            stringBuilder.replace(indexToChange, indexToChange+changeLength, " and ");
        }
        return stringBuilder.toString();
    }
}
