package pl.codementors.homeworks.adapter.car;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.codementors.homeworks.adapter.Car;

import java.io.PrintStream;

import static org.mockito.Matchers.contains;;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class CarTest {

    private Car car;

    @Mock
    private PrintStream out;

    @Before
    public void setUp(){
        car = new Car();
    }

    @Test
    public void shouldStart(){
        System.setOut(out);

        car.start();

        verify(out).println(contains("preparing"));
        verify(out).println(contains("started"));

    }

    @Test
    public void shouldStop(){
        System.setOut(out);

        car.stop();

        verify(out).println(contains("stopped"));
    }


}
