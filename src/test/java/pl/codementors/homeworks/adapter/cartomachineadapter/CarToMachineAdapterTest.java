package pl.codementors.homeworks.adapter.cartomachineadapter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import pl.codementors.homeworks.adapter.Car;
import pl.codementors.homeworks.adapter.CarToMachineAdapter;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class CarToMachineAdapterTest {

    private CarToMachineAdapter carToMachineAdapter;

    @Mock
    private Car car;

    @Before
    public void setUp(){
        carToMachineAdapter = new CarToMachineAdapter(car);
    }

    @Test
    public void shouldRun(){

        boolean run = carToMachineAdapter.run();

        assertThat(run).isTrue();
    }

    @Test
    public void shouldStop(){

        boolean stop = carToMachineAdapter.stop();

        assertThat(stop).isTrue();
    }


}
