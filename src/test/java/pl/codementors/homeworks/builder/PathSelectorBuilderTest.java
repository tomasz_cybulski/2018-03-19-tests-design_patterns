package pl.codementors.homeworks.builder;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PathSelectorBuilderTest {

    private PathSelectorBuilder pathSelectorBuilder;

    @Before
    public void setUp() throws Exception {
        pathSelectorBuilder = new PathSelectorBuilder();
    }

    @Test
    public void withNoParameters(){
        String result = "/div/";

        String path = pathSelectorBuilder.build();

        assertThat(path).isEqualTo(result);
    }

    @Test
    public void withFormGroupParameter(){
        String formGroup = "form-group";
        String result = "/div[@class='"+formGroup+"']/";

        String path = pathSelectorBuilder
                .withFormGroup(formGroup)
                .build();

        assertThat(path).isEqualTo(result);
    }

    @Test
    public void withSpanParameter(){
        String span = "span";
        String result = "/div/span";

        String path = pathSelectorBuilder
                .withSpan(span)
                .build();

        assertThat(path).isEqualTo(result);
    }

    @Test
    public void withFormGroupAndSpanParameters(){
        String formGroup = "form-group";
        String span = "span";
        String result = "/div[@class='"+formGroup+"']/span";

        String path = pathSelectorBuilder
                .withFormGroup(formGroup)
                .withSpan(span)
                .build();

        assertThat(path).isEqualTo(result);
    }

    @Test
    public void withUserMessageParameter(){
        String userMessage = "user-message";
        String result = "/div/";

        String path = pathSelectorBuilder
                .withUserMessage(userMessage)
                .build();

        assertThat(path).isEqualTo(result);
    }

    @Test
    public void withFormGroupAndUserMessageParameter(){
        String formGroup = "form-group";
        String userMessage = "user-message";
        String result = "/div[@class='"+formGroup+"']/";

        String path = pathSelectorBuilder
                .withFormGroup(formGroup)
                .withUserMessage(userMessage)
                .build();

        assertThat(path).isEqualTo(result);
    }

    @Test
    public void withSpanAndUserMessageParameters(){
        String span = "span";
        String userMessage = "user-message";
        String result = "/div/"+span+"[@id='"+userMessage+"']";

        String path = pathSelectorBuilder
                .withSpan(span)
                .withUserMessage(userMessage)
                .build();

        assertThat(path).isEqualTo(result);
    }

    @Test
    public void withFormGroupAndSpanAndUserMessageParameters(){
        String formGroup = "form-group";
        String span = "span";
        String userMessage = "user-message";
        String result = "/div[@class='"+formGroup+"']/"+span+"[@id='"+userMessage+"']";

        String path = pathSelectorBuilder
                .withFormGroup(formGroup)
                .withSpan(span)
                .withUserMessage(userMessage)
                .build();

        assertThat(path).isEqualTo(result);
    }

    @Test
    public void withFormControlParameter(){
        String formControl = "form-control";
        String result = "/div/";

        String path = pathSelectorBuilder
                .withFormControl(formControl)
                .build();

        assertThat(path).isEqualTo(result);
    }

    @Test
    public void withFormGroupAndFormControlParameters(){
        String formGroup = "form-group";
        String formControl = "form-control";
        String result = "/div[@class='"+formGroup+"']/";

        String path = pathSelectorBuilder
                .withFormGroup(formGroup)
                .withFormControl(formControl)
                .build();

        assertThat(path).isEqualTo(result);
    }

    @Test
    public void withSpanAndFormControlParameters(){
        String span = "span";
        String formControl = "form-control";
        String result = "/div/"+span+"[@class='"+formControl+"']";

        String path = pathSelectorBuilder
                .withSpan(span)
                .withFormControl(formControl)
                .build();

        assertThat(path).isEqualTo(result);
    }

    @Test
    public void withSpanAndUserMessageAndFormControlParameters(){
        String span = "span";
        String userMessage = "user-message";
        String formControl = "form-control";
        String result = "/div/"+span+"[@id='"+userMessage+"' and @class='"+formControl+"']";

        String path = pathSelectorBuilder
                .withSpan(span)
                .withUserMessage(userMessage)
                .withFormControl(formControl)
                .build();

        assertThat(path).isEqualTo(result);
    }

    @Test
    public void withFormGroupAndSpanAndUserMessageAndFormControlParameters(){
        String formGroup = "form-group";
        String span = "span";
        String userMessage = "user-message";
        String formControl = "form-control";
        String result = "/div[@class='"+formGroup+"']/"+span+"[@id='"+userMessage+"' and @class='"+formControl+"']";

        String path = pathSelectorBuilder
                .withFormGroup(formGroup)
                .withSpan(span)
                .withUserMessage(userMessage)
                .withFormControl(formControl)
                .build();

        assertThat(path).isEqualTo(result);
    }
}
